<?php

/**
 * @file
 * Command: uw-user-check.
 */

use Drush\Log\LogLevel;

/**
 * Implements hook_drush_command().
 */
function users_drush_command() {
  $items = [];
  $items['uw-user-check'] = [
    'description' => 'Checks that UW users accounts have usernames which match their CAS name and matching GUIDs.',
  ];
  return $items;
}

/**
 * Implements drush_hook_COMMAND().
 */
function drush_users_uw_user_check() {
  if (drush_drupal_major_version() > 7) {
    return drush_log(dt('Drupal 8 and above not supported.'), LogLevel::ERROR);
  }

  // Get all user IDs on the site.
  $query = 'SELECT uid FROM {users} ORDER BY uid';
  $result = db_query($query);
  $uids = $result->fetchCol();
  $uids = array_map('intval', $uids);

  // Iterate.
  drush_log(dt('Checking @count users...', ['@count' => count($uids)]), LogLevel::OK);
  foreach ($uids as $uid) {
    $ok = TRUE;

    // Skip built-in users.
    if ($uid < 2) {
      continue;
    }

    // Load the Drupal user.
    $user = user_load($uid);
    $guid_drupal = (string) $user->field_uw_user_guid['und'][0]['value'];

    // Skip non-CAS account.
    if (!$user->cas_name && !$guid_drupal) {
      continue;
    }

    // If it has a CAS name but it doesn't match, error.
    if ($user->cas_name && ($user->name !== $user->cas_name)) {
      drush_log(dt('CAS name does not match.'), LogLevel::ERROR);
      $ok = FALSE;
    }

    // GUID will be filled-in on next login.
    if ($ok && $user->cas_name && !$guid_drupal) {
      continue;
    }

    // Missing CAS name.
    if (!$user->cas_name && $guid_drupal) {
      drush_log(dt('CAS name missing; has GUID.'), LogLevel::ERROR);
      $ok = FALSE;
    }

    // LDAP lookup user.
    if ($ok) {
      $ldap = uw_ldap_lookup($user->cas_name, ['objectguid']);
      $guid_ldap = (string) $ldap['objectguid'][0];

      // If it has a GUID but it doesn't match, error.
      if ($guid_drupal && $guid_ldap && ($guid_drupal !== $guid_ldap)) {
        $args = [
          '@guid_drupal' => $guid_drupal,
          '@guid_ldap' => $guid_ldap,
        ];
        drush_log(dt('GUID does not match (Drupal: @guid_drupal; LDAP: @guid_ldap).', $args), LogLevel::ERROR);
        $ok = FALSE;
      }
    }

    // Output username if error.
    if (!$ok) {
      $args = [
        '@uid' => $uid,
        '@user_name' => $user->name,
      ];
      drush_log(dt('User @uid: @user_name', $args), LogLevel::ERROR);
    }
  }
  return drush_log(dt('uw-user-check complete'), LogLevel::SUCCESS);
}
