<?php

/**
 * @file
 * Command: pm-uninstall-disabled.
 */

use Drush\Log\LogLevel;

/**
 * Implements hook_drush_command().
 */
function pmud_drush_command() {
  $items = [];
  $items['pm-uninstall-disabled'] = [
    'description' => 'Uninstall all disabled modules.',
    'aliases' => ['pmud'],
  ];
  return $items;
}

/**
 * Implements drush_hook_COMMAND().
 */
function drush_pmud_pm_uninstall_disabled() {
  if (drush_drupal_major_version() > 7) {
    return drush_log(dt('Drupal 8 does not support disabling modules, thus there are never disabled modules to uninstall.'), LogLevel::OK);
  }

  // Get a list of disabled, installed modules. Based on system.admin.inc.
  $all_modules = system_rebuild_module_data();
  $disabled_modules = [];
  foreach ($all_modules as $name => $module) {
    if (empty($module->status) && $module->schema_version > SCHEMA_UNINSTALLED) {
      $disabled_modules[] = $name;
    }
  }

  // Inform the user which modules will be uninstalled.
  if (empty($disabled_modules)) {
    return drush_log(dt('There were no modules that could be uninstalled.'), LogLevel::OK);
  }
  else {
    drush_print(dt('The following modules will be uninstalled: !modules', ['!modules' => implode(', ', $disabled_modules)]));
    if (!drush_confirm(dt('Do you really want to continue?'))) {
      return drush_user_abort();
    }
  }

  // Uninstall the modules.
  $success = drupal_uninstall_modules($disabled_modules);

  if (!$success) {
    return drush_log(dt('Uninstall failed.'), LogLevel::ERROR);
  }

  // Inform the user of final status.
  foreach ($disabled_modules as $module) {
    drush_log(dt('!module was successfully uninstalled.', ['!module' => $module]), LogLevel::OK);
  }
}
